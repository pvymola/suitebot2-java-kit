package suitebot2.game;

public class Player
{
	public final int id;
	public final String name;

	public Player(int id, String name)
	{
		this.id = id;
		this.name = name;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Player player = (Player) o;

		return id == player.id;

	}

	@Override
	public int hashCode()
	{
		return id;
	}

	@Override
	public String toString()
	{
		return "Player{" +
				       "id=" + id +
				       ", name='" + name + '\'' +
				       '}';
	}
}
