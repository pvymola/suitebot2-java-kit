package suitebot2.awesome;

import suitebot2.ai.BotAi;
import suitebot2.game.*;

import java.util.List;

public class AwesomeAi implements BotAi
{
    private int myId;
    private Point myPosition;
    private List<Player> players;
    private GamePlan gamePlan;
    private int moves;
    private String lastMove;
    private String attemptedMove;
    private int cycles;
    private String[] currentMoveOrder;

   // private static final String[] defaultMovesOrder = new String[] {"U", "U", "R", "R", "D", "D", "L", "D", "D", "R", "R", "U", "U", "U", "U"};
   // private static final String[] moveLeftAndStartAnew = new String[] {"L", "L", "L", "L", "L", "L", "L", "L"};


    private static final String[] startSequence = new String[] {"U", "U", "R", "R", "R", "D", "D", "D", "L", "L", "L", "D", "D", "D", "R", "R", "R", "R"};
    private static final String[] cycleSequence = new String[] {"U","U", "U", "U","U", "U", "R", "R", "R", "D", "D", "D", "L", "L", "D", "D", "D", "R", "R", "R"};
    private static final String[] upOneLevel = new String[] {"U", "U", "U", "U", "U", "U"};


    @Override
    public String initializeAndMakeMove(GameSetup gameSetup) {

        myId = gameSetup.aiPlayerId;
        gamePlan = gameSetup.gamePlan;
        players = gameSetup.players;
        moves = 0;
        cycles = 1;

        setMyStartingPosition();

        currentMoveOrder = startSequence;
        attemptedMove = currentMoveOrder[moves];
        return attemptedMove;
    }

    @Override
    public String makeMove(GameRound gameRound) {
        lastMove = myMove(gameRound.moves);
        boolean iMoved = !lastMove.equals("X");

        if (!iMoved)
            return attemptedMove;

        // count only real moves that happened
        moves++;
        attemptedMove = figureAMove();

        return attemptedMove;
    }

    private String figureAMove() {
        if (moves >= currentMoveOrder.length)
        {
            moves = 0;
            cycles++;
        }

        if (cycles == 2)
            currentMoveOrder = cycleSequence;
        else if (cycles % 8 == 0)
            currentMoveOrder = upOneLevel;
        else
            currentMoveOrder = cycleSequence;

        return currentMoveOrder[moves];
    }

    private String myMove(List<PlayerMove> moves) {
        for (PlayerMove move : moves)
            if (move.playerId == myId)
                return move.move;

        return "X";
    }

    private void setMyStartingPosition()
    {
        int myIndex = players.indexOf(new Player(myId, null));
        myPosition = gamePlan.startingPositions.get(myIndex);
    }
}
