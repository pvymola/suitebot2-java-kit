package suitebot2.json;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import suitebot2.game.GameRound;
import suitebot2.game.GameSetup;

public class JsonUtil
{
	public static JsonUtil.MessageType decodeMessageType(String json)
	{
		JsonParser parser = new JsonParser();
		JsonObject parsedJson = parser.parse(json).getAsJsonObject();
		return MessageType.valueOf(parsedJson.get("messageType").getAsString().toUpperCase());
	}

	public static GameRound decodeMovesMessage(String json)
	{
		return new Gson().fromJson(json, GameRound.class);
	}

	public static GameSetup decodeSetupMessage(String json)
	{
		return new Gson().fromJson(json, GameSetup.class);
	}

	public enum MessageType { SETUP, MOVES }
}
