package suitebot2.game;

import org.junit.Test;
import suitebot2.awesome.AwesomeAi;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by pvymola on 6/13/2016.
 */
public class SimpleTest {

    @Test
    public void testInstanceCreationAndMove() throws Exception {

        AwesomeAi ai = new AwesomeAi();

        Player player = new Player(0, "Test");
        Point position = new Point(10, 10);
        GamePlan plan = new GamePlan(20, 30, Arrays.asList(position), 1000);

        GameSetup gameSetup = new GameSetup(0, Arrays.asList(player), plan);

        String move = ai.initializeAndMakeMove(gameSetup);
        assert move.equals("U");

        PlayerMove myMove = new PlayerMove(0, "X");
        GameRound round2 = new GameRound(Arrays.asList(myMove));

        String move2 = ai.makeMove(round2);

        assert move2.equals("U");
    }

    @Test
    public void testLongerGameWorks() throws Exception {

        AwesomeAi ai = new AwesomeAi();

        Player player = new Player(0, "Test");
        Point position = new Point(10, 10);
        GamePlan plan = new GamePlan(20, 30, Arrays.asList(position), 1000);

        GameSetup gameSetup = new GameSetup(0, Arrays.asList(player), plan);

        String move = ai.initializeAndMakeMove(gameSetup);
        assert move.equals("U");

        List<String> moves = new LinkedList<>();

        for (int i = 0; i<10000; i++) {
            PlayerMove myMove = new PlayerMove(0, move);
            GameRound round2 = new GameRound(Arrays.asList(myMove));
            move = ai.makeMove(round2);
            moves.add(move);
        }

        int i = 0;
        assert !moves.isEmpty();

        assert moves.size() == 10000;
    }

}
